using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirplaneCamera : Entity
{
    public Entity TrackedAirplane;
    public Vector3 camera_offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = TrackedAirplane.transform.position;
        transform.eulerAngles = TrackedAirplane.transform.eulerAngles;
        transform.Translate(camera_offset, Space.Self);
    }
}
