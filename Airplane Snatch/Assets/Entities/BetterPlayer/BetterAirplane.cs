using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterAirplane : Entity
{
    private Rigidbody rb;
    private Vector3 last_mouse;
    public Transform rotation_target_object;
    public float mouse_sensitivity;
    public float mouse_max;
    public float yaw_sensitivity;
    //public Vector3 gravity;
    public float thrust = 5;
    public float thrust_scale;
    public float thrust_min;
    public float thrust_no_gravity;
    public float thrust_max;
    public float movement_scale = 1;
    public Vector3 localVelocity = Vector3.zero;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //Cursor.lockState = CursorLockMode.Locked;
    }
    
    void FixedUpdate()
    {
        transform.Rotate(Vector3.up, Input.GetAxis("Horizontal") * yaw_sensitivity);
        //Vector3 mouse_diff = Input.mousePosition - last_mouse;
        transform.Rotate(Vector3.forward, -Mathf.Clamp(Input.GetAxis("Mouse X") * mouse_sensitivity, -mouse_max, mouse_max));
        transform.Rotate(Vector3.right, Mathf.Clamp(Input.GetAxis("Mouse Y") * mouse_sensitivity, -mouse_max, mouse_max));
        //transform.Rotate(Vector3.forward, mouse_diff.x * mouse_sensitivity);
        //transform.Rotate(Vector3.right, mouse_diff.y * mouse_sensitivity);
        //last_mouse = Input.mousePosition;
        //transform.position += rb.velocity * Time.deltaTime;

        thrust += Input.GetAxis("Vertical") * thrust_scale;
        thrust = Mathf.Clamp(thrust, thrust_min, thrust_max);
        //localVelocity = transform.InverseTransformVector(rb.velocity);
        //localVelocity = new Vector3(localVelocity.x, localVelocity.y, localVelocity.z);
        rb.velocity += transform.forward * thrust * movement_scale;
        rb.angularVelocity = Vector3.zero;
        float gravity_scale = 1f - Mathf.Clamp((thrust - thrust_min) / (thrust_no_gravity - thrust_min), 0, 1);
        rb.velocity += Physics.gravity * 14 * gravity_scale * Time.deltaTime;

    }

    float SimpleBellCurve(float x, float b) // b should be 1 or higher, lower = wider
    {
        return Mathf.Pow(b, -(x * x));
    }
}
