using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Airplane : Entity
{
    private Rigidbody rb;
    public float mouse_sensitivity;
    public float yaw_sensitivity; 
    //public Vector3 gravity;
    public float lift_scale;
    public float lift_threshold;
    public float lift_cap;
    public float thrust_scale;
    public Vector3 localVelocity = Vector3.zero;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate()
    {
        transform.Rotate(Vector3.up, Input.GetAxis("Horizontal") * yaw_sensitivity);
        transform.Rotate(Vector3.forward, Input.GetAxis("Mouse X") * mouse_sensitivity);
        transform.Rotate(Vector3.right, Input.GetAxis("Mouse Y") * mouse_sensitivity);
        //transform.position += rb.velocity * Time.deltaTime;

        localVelocity = transform.InverseTransformVector(rb.velocity);
        //velocity += Physics.gravity * Time.deltaTime;
        Vector3 lift = transform.up * lift_scale * localVelocity.z;
        Debug.Log(lift);
        lift = lift.magnitude > lift_threshold ? (lift * Time.deltaTime) : Vector3.zero;
        rb.velocity += lift.magnitude > lift_cap ? (lift.normalized * lift_cap) : lift;
        Vector3 thrust_force = transform.forward * Input.GetAxis("Vertical") * thrust_scale;
        rb.velocity += SimpleBellCurve(localVelocity.z, 1.001f) * thrust_force * Time.deltaTime;
        //Debug.Log(rb.velocity);
    }

    float SimpleBellCurve(float x, float b) // b should be 1 or higher, lower = wider
    {
        return Mathf.Pow(b, -(x * x));
    }
}
